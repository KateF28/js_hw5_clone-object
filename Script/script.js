// ## Задание
// Реализовать функцию полного клонирования объекта.

let obj = {
    name: "Who",
    age: 5,
    sublings: undefined,
    "has sister": false,
    hasDog: {
        name: "Puppy",
        age: 2,
        hasToy: {
        	ball: 'red',
        },
    },
};


let objCloned = Object.assign({}, obj);
let objCloning = (insideKey) => {
    for (let key in obj) {
        if (typeof key === 'object' && key !== null) {
            // return 
            objCloned[key] = Object.assign(objCloned[objCloning(obj[key])], obj[key]);
        } else {
            objCloned[key] = Object.assign(objCloned[key], obj[key]);
        };
        objCloning(obj[key]);
    };
};


objCloned.name = "boy";
objCloned.hasDog = "has";
objCloned.hasDog.hasToy = "has";

console.log(objCloned);
console.log(obj.name);
console.log(obj.hasDog);
console.log(obj.hasDog.hasToy);